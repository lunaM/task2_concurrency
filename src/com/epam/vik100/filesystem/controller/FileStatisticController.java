package com.epam.vik100.filesystem.controller;

import java.nio.file.NoSuchFileException;
import java.util.concurrent.Executors;

import com.epam.vik100.filesystem.model.FilesStatistic;
import com.epam.vik100.filesystem.model.FilesStatisticExecutors;
import com.epam.vik100.filesystem.model.FilesStatisticFJ;
import com.epam.vik100.filesystem.model.FilesStatisticParallel;
import com.google.common.collect.ConcurrentHashMultiset;

public class FileStatisticController {
	public static void main(String[] args) throws NoSuchFileException {

		String path = "D:\\vik";

		System.out.println("Without multithreading implementation");
		FilesStatistic statistic = new FilesStatistic(path);
		statistic.collectStatistic();
		System.out.println(statistic);

		System.out.println("\nWith parallel streams");
		FilesStatisticParallel statisticParallel = new FilesStatisticParallel(path);
		statisticParallel.collectStatistic();
		System.out.println(statisticParallel);

		System.out.println("\nWith fork/join implementation");
		FilesStatisticFJ forkJoin = new FilesStatisticFJ(ConcurrentHashMultiset.create(), path);
		forkJoin.collectStatistic();
		System.out.println(forkJoin);

		System.out.println("\nWith ExecutorService");
		FilesStatisticExecutors fse = new FilesStatisticExecutors(ConcurrentHashMultiset.create(), path,
				Executors.newCachedThreadPool());
		fse.collectStatistic();
		System.out.println(fse);
		
		statistic.printTopTen();
	}
}
