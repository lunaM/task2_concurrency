package com.epam.vik100.filesystem.model;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import com.google.common.collect.HashMultiset;

public class FilesStatistic {
	private HashMultiset<String> filesAndFolders;
	private String path;

	public FilesStatistic(String path) {
		this.path = path;
	}

	public void collectStatistic() {
		filesAndFolders = HashMultiset.create();
		Path start = Paths.get(path);
		try (Stream<Path> stream = Files.walk(start)) {
			stream.forEach(pathelement -> {
				filesAndFolders.add(pathelement.getFileName().toString());
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void printTopTen() {
		System.out.println("*  *  *  *  *  *");
		System.out.println("\n\nTop ten names");
		System.out.println("*  *  *  *  *  *");
		StatisticCollector.getTopTenNames(filesAndFolders).entrySet().forEach(entry -> {
			System.out.println(entry.getValue() + " " + entry.getKey());
		});
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("\n* * * PATH - ");
		result.append(path);
		result.append(" * * *\n");
		result.append("Total number - ");
		result.append(filesAndFolders.size());
		result.append("\nDistinct - ");
		result.append(filesAndFolders.entrySet().size());
		return result.toString();
	}
}
