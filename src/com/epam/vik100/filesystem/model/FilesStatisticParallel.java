package com.epam.vik100.filesystem.model;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import com.google.common.collect.ConcurrentHashMultiset;

public class FilesStatisticParallel {
	private ConcurrentHashMultiset<String> filesAndFolders;
	private String path;

	public FilesStatisticParallel(String path) {
		this.path = path;
		filesAndFolders = ConcurrentHashMultiset.create();
	}

	public void collectStatistic() {
		filesAndFolders = ConcurrentHashMultiset.create();
		Path start = Paths.get(path);
		try (Stream<Path> stream = Files.walk(start)) {
			stream.parallel().forEach(pathelement -> {
				filesAndFolders.add(pathelement.getFileName().toString());
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("\n* * * PATH - ");
		result.append(path);
		result.append(" * * *\n");
		result.append("Total number - ");
		result.append(filesAndFolders.size());
		result.append("\nDistinct - ");
		result.append(filesAndFolders.entrySet().size());
		return result.toString();
	}
}
