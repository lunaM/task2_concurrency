package com.epam.vik100.filesystem.model;

import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Multiset;

public class StatisticCollector {
	public static Map<String, Integer> getTopTenNames(Multiset<String> multiset) {
		Map<String, Integer> result = new HashMap<>(10);
		multiset.stream().distinct().forEach(element -> {
			if (result.size() < 10) {
				result.put(element, multiset.count(element));
			} else {
				for (Map.Entry<String, Integer> entry : result.entrySet()) {
					if (entry.getValue() < multiset.count(element)) {
						result.remove(entry.getKey());
						result.put(element, multiset.count(element));
						break;
					}
				}
			}
		});
		return result;
	}
}
