package com.epam.vik100.filesystem.model;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

import com.google.common.collect.ConcurrentHashMultiset;

public class FilesStatisticFJ extends RecursiveAction {
	private final ConcurrentHashMultiset<String> filesAndDirNames;
	private final Path currentPath;

	public FilesStatisticFJ(final ConcurrentHashMultiset<String> filesAndDirNames, final String currentPath) {
		this.filesAndDirNames = filesAndDirNames;
		this.currentPath = Paths.get(currentPath);
	}

	@Override
	protected void compute() {
		File file = new File(currentPath.toString());
		if (file.isDirectory()) {
			filesAndDirNames.add(currentPath.getFileName().toString());
			Arrays.stream(file.listFiles()).forEach(
					elem -> ForkJoinTask.invokeAll(new FilesStatisticFJ(filesAndDirNames, elem.getAbsolutePath())));
		} else {
			filesAndDirNames.add(currentPath.getFileName().toString());

		}
	}

	public void collectStatistic() {
		final ForkJoinPool forkJoinPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
		forkJoinPool.invoke(new FilesStatisticFJ(filesAndDirNames, currentPath.toString()));
		forkJoinPool.shutdown();
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("\n* * * PATH - ");
		result.append(currentPath);
		result.append(" * * *\n");
		result.append("Total number - ");
		result.append(filesAndDirNames.size());
		result.append("\nDistinct - ");
		result.append(filesAndDirNames.entrySet().size());
		return result.toString();
	}
}
