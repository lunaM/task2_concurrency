package com.epam.vik100.filesystem.model;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.collect.ConcurrentHashMultiset;

public class FilesStatisticExecutors {
	private final ConcurrentHashMultiset<String> filesAndDirNames;
	private final Path startPath;
	private final ExecutorService executorService;
	private AtomicInteger amount = new AtomicInteger(0);

	class ListFiles implements Callable<Void> {
		private final Path currentPath;

		public ListFiles(String currentPath) {
			this.currentPath = Paths.get(currentPath);
		}

		@Override
		public Void call() throws InterruptedException, ExecutionException {
			amount.incrementAndGet();
			File file = new File(currentPath.toString());
			if (file.isDirectory()) {
				List<Future<Void>> f = new ArrayList<>();
				filesAndDirNames.add(currentPath.getFileName().toString());
				Arrays.stream(file.listFiles()).forEach(elem -> {
					try {
						f.add(executorService.submit(new ListFiles(elem.getAbsolutePath())));
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
				for (Future<Void> fu : f) {
					fu.get();
				}
			} else {
				filesAndDirNames.add(currentPath.getFileName().toString());
			}
			amount.decrementAndGet();
			return null;
		}

	}

	public FilesStatisticExecutors(final ConcurrentHashMultiset<String> filesAndDirNames, String startPath,
			final ExecutorService executorService) {
		this.filesAndDirNames = filesAndDirNames;
		this.startPath = Paths.get(startPath);
		this.executorService = executorService;
	}

	public void collectStatistic() {
		try {
			executorService.submit(new ListFiles(startPath.toString())).get();
			while (isWorking()) {
			}
			executorService.shutdown();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

	}

	public boolean isWorking() {
		return amount.get() != 0;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("\n* * * PATH - ");
		result.append(startPath);
		result.append(" * * *\n");
		result.append("Total number - ");
		result.append(filesAndDirNames.size());
		result.append("\nDistinct - ");
		result.append(filesAndDirNames.entrySet().size());
		return result.toString();
	}

}
